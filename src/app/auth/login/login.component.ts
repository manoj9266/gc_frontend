import { Component, OnInit,AfterViewInit,NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { storage } from 'firebase';
import { ApiService } from 'src/app/api.service';
import { StorageService } from 'src/app/storage.service';
import {AuthService} from '../auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 isLoading:boolean=false;
  constructor(private readonly auth:AuthService,private router:Router,private apiService:ApiService,private storage:StorageService,private ngZone:NgZone) { 
    console.log("insite login component");    
    this.auth.signinStatus.subscribe(signin=>{
      console.log("login component sigining:"+signin);
      if(signin){
       // this.router.navigate(['/contacts']); 
      }
    })
  }

  ngOnInit(): void {
    this.auth.loadingChange.subscribe(loading=>{
        
        console.log("loading3="+this.isLoading);
        // if(!loading){
        //   this.router.navigate(['/login']);
        // }
      })
  }
  
  ngAfterViewInit(): void {
   
}

fetchContacts(){    
 //   this.auth.fetchContacts();
}

handleAuthClick() {
  //gapi.auth2.getAuthInstance().signIn();
  this.auth.gooogleSignIn().then(res=>{
    let email = res.tt && res.tt.bu?res.tt.bu:"";
      let name = res.tt && res.tt.Ad?res.tt.Ad:"";
      let photo = res.tt && res.tt.jK?res.tt.jK:"";
      let ob = {name,email,photo};
      console.log(ob);
      this.isLoading = true;
      this.apiService.register(ob).subscribe(user=>{
        this.storage.login(user);
        //this.router.navigate(['/contacts']); 
        this.ngZone.run(() => this.router.navigateByUrl("contacts")).then();
      })
  });
}

 handleSignoutClick() {
 this.auth.googleSignOut();
}
}
