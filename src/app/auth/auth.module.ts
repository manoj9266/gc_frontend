import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
import {LoginComponent} from './login/login.component'
import {AuthComponent} from './auth.component'
import { environment } from 'src/environments/environment';
//import { AuthService } from './auth.service';
import { HttpClientModule } from '@angular/common/http';
import { AuthGuard } from '../auth.guard';
import {SharedModule} from '../shared/shared.module';
//import { AuthGuard } from '../auth.guard';
 //import {GoogleApiModule, GoogleApiService, GoogleAuthService, NgGapiClientConfig, NG_GAPI_CONFIG,GoogleApiConfig} from "ng-gapi";
// let gapiClientConfig: NgGapiClientConfig = {
//   client_id: "372063809670-fvbs4n6op7eamkincjvd265b159aaq1c.apps.googleusercontent.com",
//   discoveryDocs: ["https://analyticsreporting.googleapis.com/$discovery/rest?version=v4"],
//   ux_mode: "redirect",
//   redirect_uri: "https://ng-gapi-example.stackblitz.io/redirect",
//   scope: [
//       "https://www.googleapis.com/auth/userinfo.profile"
//   ].join(" ")
// };
@NgModule({
  declarations: [LoginComponent,AuthComponent],
  imports: [
    HttpClientModule,    
    CommonModule,
    SharedModule,
    RouterModule.forRoot([           
      {path:'login',component:AuthComponent,canActivate:[AuthGuard]},      
    ]),
    // GoogleApiModule.forRoot({
    //   provide: NG_GAPI_CONFIG,
    //   useValue: gapiClientConfig
    // })
  ],
  providers:[]
})
export class AuthModule { }
