import { Injectable,OnInit,NgZone } from '@angular/core';
import { Observable, of,from,Subject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { StorageService } from '../storage.service';
import {environment} from '../../environments/environment'
declare const gapi: any;
@Injectable({
  providedIn: 'root'
})

export class AuthService { 
private loading = true;
loadingChange: Subject<boolean> = new Subject();
signinStatus: Subject<boolean> = new Subject();
constructor(private http: HttpClient,private router:Router,private storage:StorageService,private ngZone:NgZone) {
  //console.log("inside auth service");  
  this.waitForLoad();
}
isLoggedIn():boolean{
  return this.storage.isLoggedIn();
}
private emit(loading) {
  this.loadingChange.next(loading);
}
waitForLoad(){
let timer = setInterval(()=>{
  if(typeof gapi=="object"){
        clearInterval(timer);
        this.initClient(this);
  }
},100)
  
}

  ngOnInit(){
    //console.log("ngOnInit");
  }
 initClient(outerThis){
   
    gapi.load('client:auth2', () => {
     // console.log("AuthService loaded");
      gapi.client.init({
          apiKey: environment.firebase.apiKey,
          discoveryDocs: ["https://www.googleapis.com/discovery/v1/apis/people/v1/rest"],
          clientId: environment.auth.clientId,
          scope: 'profile email https://www.googleapis.com/auth/contacts.readonly'
      }).then(() => {
       // console.log("AuthService init");
        outerThis.loading =false;     
        outerThis.emit(outerThis.loading);
                
        gapi.auth2.getAuthInstance().isSignedIn.listen((status)=>{
          outerThis.updateSigninStatus(outerThis,status);
        });
      }).catch(err=>{
        console.log(err);
      })
  });
        
  }
  updateSigninStatus(outerThis,isSignedIn){
    console.log("authservice sign:"+isSignedIn);
    outerThis.signinStatus.next(isSignedIn);    
    localStorage.setItem("signin_status",isSignedIn?"true":"false");       
  }
  gooogleSignIn() {
    return gapi.auth2.getAuthInstance().signIn();    
  }

  fetchContacts(ob){
       
    if(!this.loading){
      let pageToken = ob.pageToken?ob.pageToken:"";
      let pageSize = ob.pageSize?ob.pageSize:20;

    return  gapi.client.people.people.connections.list({
      resourceName:'people/me',
      pageSize: pageSize,
      pageToken:pageToken,
      personFields: 'names,phoneNumbers,emailAddresses,photos'})
    }else {
      return Promise.reject("gapi not initilized!!");
    }

  }
  
  googleSignOut(){
    
    gapi.auth2.getAuthInstance().signOut().then(res=>{
      console.log("signed out");
       localStorage.setItem("signin_status","");      
       this.storage.logout();
      this.ngZone.run(() => this.router.navigateByUrl("login")).then();
      
    });
  }
  isLoading():boolean{
    return this.loading;
  }
  isLoadingSubscriber():Observable<boolean>{
    return of(this.loading);
  }

}
