import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';
import {AuthService} from '../auth/auth.service'
@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css']
})
export class LoadingComponent implements OnInit {
 isLoading:boolean =true;
  constructor(private auth:AuthService,private router:Router) { }
  //constructor(){

  //}

  ngOnInit(): void {
    // console.log("inside loading component");
    this.auth.loadingChange.subscribe(loading=>{
      this.isLoading = loading;
      console.log("loading1="+loading);
      if(!loading){
        this.router.navigate(['/login']);
      }
    })
  }


}
