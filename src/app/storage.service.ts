import { Injectable } from '@angular/core';
import { IUser } from './IUser';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

constructor() { }
 login(user:IUser){
   localStorage.setItem("user_id",user._id)   
 }
 logout(){
  localStorage.setItem("user_id","")   
 }
 getUserId(){
  return localStorage.getItem("user_id");
 }
 isLoggedIn(){
  return (localStorage.getItem("user_id") && localStorage.getItem("user_id").length>=10)?true:false; 
 }
}
