export interface IUser {
    name?:string,
    email?:string,
    photo?:string,
    _id?:string
}
