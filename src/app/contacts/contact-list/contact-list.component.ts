import { Component, OnInit,Input,AfterViewInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import {Contact} from '../../Contact';
@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {
  pageToken = "";
  fetching = false;
  all_contacts_fetched = false;
  currentContactsCount:number=0;
  totalContact:number;    
  contacts  = new BehaviorSubject([]);
  pending = true;
  //totalContact:number ;
  constructor(public auth:AuthService) { 
    console.log("inside contact-list component");
    
  }

  ngOnInit(): void {
    console.log("inside contact-list onInit");
    this.auth.loadingChange.subscribe(loading=>{
      console.log("contact-list loading:"+loading);
      if(!loading){        
        //this.auth.fetchContacts();
        this.fetchContacts();
      }
    });
  }
  onScroll(e) {
    //console.log('scrolled!!', e);
    
   
     this.fetchContacts();   
    //this.contacts = [...this.contacts, ...moreBoxes];
    
  }
  ngAfterViewInit() {
    console.log("contact-list afterViewInit");
    let timer = setInterval(()=>{
      
      if(this.all_contacts_fetched){
        clearInterval(timer);
      }else if(this.pending){
        this.fetchContacts();
      }
    },2000)
    
    
  
  }
  
  fetchContacts(){
    //console.log("fetching:"+this.fetching);
    if(this.auth.isLoading()){
       console.log("loading..");
       this.pending = true;
       return;
    }else
    if(this.fetching){
      console.log("already in progress");
      this.pending = true;
      return;
    }else if(this.all_contacts_fetched){
      console.log("no more contacts");
      this.pending = true;
      return ;
    }
    this.pending = false;
    this.fetching = true;
    this.auth.fetchContacts({pageToken:this.pageToken,pageSize:20}).then(
      (res) => {
        //console.log("Res: " + JSON.stringify(res)); 
        
        this.totalContact  = +res.result.totalItems;        
        this.pageToken = res.result.nextPageToken;
        let connections = res.result.connections;        
        let items :Contact[] = [];

        //formatting contacts
        connections.forEach((person) => {
          //console.log(person.names);          
          if(person.names && person.phoneNumbers){
              let phone = person.phoneNumbers.length>0?person.phoneNumbers[0].value:"";
             let name = person.names.length>0?person.names[0].displayName:"";
             let photo = person.photos.length>0?person.photos[0].url:"";
             let contact:Contact ={name,phone,photo};  
             
             items.push(contact);               
          }                    
        });


        //add fetched contacts to current list
        const currentContacts = this.contacts.getValue();
        this.currentContactsCount += connections.length;
          this.contacts.next(currentContacts.concat(items));          

        //check if all contacts fetched  
        if(this.currentContactsCount>=this.totalContact){
           this.all_contacts_fetched = true;
          }
          this.fetching = false;
          console.log("fetching2:"+this.fetching);
          // console.log("currentCount="+(+currentContacts.length+(+items.length)));
          
    },
    error => console.log("ERROR " + JSON.stringify(error))
    )
  }


}
