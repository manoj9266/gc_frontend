import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { AuthService } from 'src/app/auth/auth.service';
import { IUser } from 'src/app/IUser';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
 user:IUser;
  constructor(private auth:AuthService,private apiService:ApiService) { }

  ngOnInit(): void {
    this.apiService.getUser().subscribe(user=>{
      console.log(user);
    this.user = user;
    })
  }
  logout(){
    this.auth.googleSignOut();
  }

}
