import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactsComponent } from './contacts.component';
import { ContactListComponent } from './contact-list/contact-list.component';
import {AuthGuard} from '../auth.guard';
import {ActivatedRoute, Router, RouterModule} from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
  declarations: [HeaderComponent,ContactsComponent,ContactListComponent],
  imports: [
    CommonModule,
    InfiniteScrollModule,
    RouterModule.forRoot([           
      {path:'contacts',component:ContactsComponent,canActivate:[AuthGuard]},      
    ]),
  ]
})
export class ContactsModule { }
