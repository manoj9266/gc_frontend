import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ActivatedRoute, Router, RouterModule} from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { ContactsModule } from './contacts/contacts.module';
import {AuthGuard} from './auth.guard';

import { LoadingComponent } from './loading/loading.component';
import { AuthService } from './auth/auth.service';
import { ApiService } from './api.service';
import { StorageService } from './storage.service';
//import {GoogleApiModule, GoogleApiService, GoogleAuthService, NgGapiClientConfig, NG_GAPI_CONFIG,GoogleApiConfig} from "ng-gapi";
@NgModule({
  declarations: [
    AppComponent,    
    LoadingComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AuthModule,
    ContactsModule,
    RouterModule.forRoot([                 
      //{path:'contacts',component:ContactsComponent,canActivate:[AuthGuard]},          
      {path:'loading',component:LoadingComponent},
      {path:'**',redirectTo:'/loading'}
    ]),
  ],
  providers: [AuthGuard,AuthService,StorageService,ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
