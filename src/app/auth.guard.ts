import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree,Router,ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService:AuthService,private router:Router){

  }
  isLoggedIn():boolean{
    return this.authService.isLoggedIn();
    // if(localStorage.getItem("token")!=null && localStorage.getItem("token").length>=5){
    //   return true;
    //   }              
    //     console.log("redirect login ");
    //     this.router.navigate(['login']);
    //      return false;      
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    //state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      state: RouterStateSnapshot): boolean {
        //let aRoute:any = route.snapshot;
        
      console.log("Auth Guard load:"+this.authService.isLoading()+" state: "+state.url+" next:"+next.url);
      // if(this.authService.isLoading()){

      //   console.log(" redirect1 from :"+state.url+"  to loading");
      //   this.router.navigate(['/loading']);
      //   return false;
      // }else 
      if(this.isLoggedIn()){
        if(state.url!="/contacts"){
          console.log(" redirect2 from :"+state.url+"  to contacts");
          this.router.navigate(['/contacts']);
          return false;
        }
        console.log(" no redirect:"+state.url);
        return true;
      }else{
        if(state.url!="/login"){
          console.log(" redirect3 from :"+state.url+"  to login");
          this.router.navigate(['/login']);
          return false;
        }
        console.log(" no redirect:"+state.url);
return true;
      }        
    
      
    //return true;
  }
  
}
