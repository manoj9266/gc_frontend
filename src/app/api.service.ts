import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import {IUser} from './IUser';
import { StorageService } from './storage.service';
import {environment} from '../environments/environment'
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiUrl = environment.config.base_url+"/user";
  
constructor(private http:HttpClient,private storage:StorageService) {
  console.log("apiUrl:"+this.apiUrl);
 }
register(user:IUser):Observable<IUser>{
  return this.http.post<IUser>(this.apiUrl,user)
         
}
getUser():Observable<IUser>{ 
  //const params:string = "id=5f48a27cb79209516c826049" ;
  const params:string = "user_id="+this.storage.getUserId();
  return this.http.get<IUser>(this.apiUrl+"?"+params);
         
}

}
