// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {  
    apiKey: 'AIzaSyBl9jMZ-aJfTV_gHWlOEN-qcu913dTK5yc',
    authDomain: 'contact-app-deb0d.firebaseapp.com',    
    projectId: 'contact-app-deb0d',        
  },
  auth:{
    clientId:"37372466453-pjo6m8qee5a47hoh1okdu7jpuff40fnd.apps.googleusercontent.com",    
  },
  config:{
    base_url :"http://localhost:3003"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
